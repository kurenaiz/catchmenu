# catchmenu

simple catchme script, it lists every music on your playlist and plays the one you select.
the base script is compatible with your stock unpatched dmenu, you might want to edit the script
or override some of the environment variables to add cool functionalities to catchemenu. Some examples below.

## Interesting things
### Preselect currently playing music
if you have the [preselect patch](https://tools.suckless.org/dmenu/patches/preselect/), you can open
catchmenu with the currently playing music selected.

```shell
# add " -n $(catchme format ';playlist-pos;') " to dmenu call
chosen=$(printf "$list" | dmenu -F -n $(catchme format ';playlist-pos;') -i -l 15)
```

### Printindex for reliable and faster selection
if you have the [printindex patch](https://tools.suckless.org/dmenu/patches/printindex/), you can
instead of greping for the song, pass the index directly to catchme, this is way faster than grepping
and avoids ambiguous situations like Theme_of_Laura.opus and aura.opus conflicting.

```
use the catchme-with-printindex instead (rename it)
```
